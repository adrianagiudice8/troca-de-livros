package br.com.itau.trocadelivros.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.itau.trocadelivros.models.TrocaLivros;
import br.com.itau.trocadelivros.viewobjects.Livro;

@FeignClient(name="livro")
public interface LivroClient {

	@GetMapping("/{id}")
	public Livro buscarPorId(@PathVariable int id);
	
	@PostMapping("/atualizar/{idLivro}/{idUsuarioProprietario}")
	public boolean atualizarProprietario(@PathVariable int idLivro, @PathVariable int idUsuarioProprietario);
}
