package br.com.itau.trocadelivros.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.trocadelivros.exceptions.TrocaException;
import br.com.itau.trocadelivros.models.TrocaLivros;
import br.com.itau.trocadelivros.viewobjects.Livro;

@Service
public class TrocaLivrosService {
	@Autowired
	LivroService livroService;

	public boolean trocarLivros(TrocaLivros trocaEntrada) throws TrocaException {

		Livro livroEnviado = 
				verificarDisponibilidadePropriedadeLivro(trocaEntrada.getIdLivroEnviado(),trocaEntrada.getIdUsuarioEnviado());
		
		Livro livroSolicitado = 
				verificarDisponibilidadePropriedadeLivro(trocaEntrada.getIdLivroSolicitado(),trocaEntrada.getIdUsuarioSolicitado());
		
		return 
			livroService.atualizarProprietario
			(
				livroEnviado.getId(), 
				livroSolicitado.getIdUsuario()
			) &&
			livroService.atualizarProprietario
			(
				livroSolicitado.getId(), 
				livroEnviado.getIdUsuario()
			);

	}
	
	private Livro verificarDisponibilidadePropriedadeLivro(int idLivro, int idUsuario) throws TrocaException
	{
		Optional<Livro> livroOptional = 
				livroService.buscar(idLivro);
		
		if (!livroOptional.isPresent())
		{
			throw new TrocaException("O usuário " + idUsuario + " não possui o livro");
		}
		else if(!livroOptional.get().isDisponivel())
		{
			throw new TrocaException("O livro do usuário " + idUsuario + " não está disponivel para troca.");
		}
		else if (livroOptional.get().getIdUsuario() != idUsuario)
		{
			throw new TrocaException("O livro informado pelo usuário " + idUsuario + " não pertence a ele.");
		}
		
		return livroOptional.get();
	}


	

}
