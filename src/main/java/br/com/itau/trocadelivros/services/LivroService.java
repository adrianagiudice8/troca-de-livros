package br.com.itau.trocadelivros.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.trocadelivros.clients.LivroClient;
import br.com.itau.trocadelivros.viewobjects.Livro;

@Service
public class LivroService {

	@Autowired
	LivroClient livroClient;
	
	public Optional<Livro> buscar(int id){
		Livro livro;
		try {
			livro = livroClient.buscarPorId(id);

		} catch (Exception e) {
			return Optional.empty();
		}
		
		return Optional.of(livro);
	}
	
	public boolean atualizarProprietario(int idLivro, int idNovoUsuarioProprietario)
	{
		return livroClient.atualizarProprietario(idLivro, idNovoUsuarioProprietario);
	}
}




