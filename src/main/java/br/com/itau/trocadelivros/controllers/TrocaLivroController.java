package br.com.itau.trocadelivros.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.trocadelivros.exceptions.TrocaException;
import br.com.itau.trocadelivros.models.TrocaLivros;
import br.com.itau.trocadelivros.services.TrocaLivrosService;

@RestController
@RequestMapping
public class TrocaLivroController {

	@Autowired
	TrocaLivrosService trocaLivrosService;
	
	@PostMapping
	public ResponseEntity trocarLivros(@RequestBody TrocaLivros troca){
		
		try {
			trocaLivrosService.trocarLivros(troca);
		} catch (TrocaException ex) {
			return ResponseEntity.status(417).body(ex.getMessage());
		}
				
		return ResponseEntity.status(201).body("Troca efetuada com sucesso");
	}

	
}
