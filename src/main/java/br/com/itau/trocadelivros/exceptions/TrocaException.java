package br.com.itau.trocadelivros.exceptions;

import com.fasterxml.jackson.databind.deser.Deserializers.Base;

public class TrocaException extends Exception {

	public TrocaException(String mensagem) 
	{
		super(mensagem);
	}
}
